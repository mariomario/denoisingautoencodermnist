Here I use a denoising autoencoder on the mnist dataset. 
28x28 pixel, 1-channel images of hand-drawn numbers. 
Uniform random noise is added. 
Deep (but fully connected) neural net trained to predict clean images from noisy ones. 
auto.py trains the autoencoder. 
load.py reads the saved model and uses it to denoise test images and saves some. 

FOR A QUICK LOOK RUN python load.py.
Make sure you have all the libraries (keras, matplotlib, numpy...).

See https://ramhiser.com/post/2018-05-14-autoencoders-with-keras/
