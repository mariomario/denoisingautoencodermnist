from IPython.display import Image, SVG
import matplotlib.pyplot as plt
import numpy as np
import keras
from keras.datasets import mnist
from keras.models import Model, Sequential
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D, Flatten, Reshape
from keras import regularizers

# Loads the training and test data sets (ignoring class labels)
(x_train, _), (x_test, _) = mnist.load_data()

# Scales the training and test data to range between 0 and 1.
max_value = float(x_train.max())
x_train = x_train.astype('float32') / max_value
x_test = x_test.astype('float32') / max_value

print x_train.shape
print x_test.shape
print x_train[0,:,:]

x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

print x_train.shape
print x_test.shape
autoencoder = keras.models.load_model("autoencoder.hdf5")

num_images = 15
np.random.seed(44)
random_test_images = np.random.randint(x_test.shape[0], size=num_images)

x_test.shape
x_test_clean = x_test
x_test = x_test + np.random.rand(10000, 28*28)

#x_train = x_train.reshape((len(x_train), 28, 28, 1))
x_test = x_test.reshape((len(x_test), 28, 28, 1))
#x_train_clean = x_train_clean.reshape((len(x_train), 28, 28, 1))
x_test_clean = x_test_clean.reshape((len(x_test), 28, 28, 1))

decoded_imgs = autoencoder.predict(x_test)

for i, image_idx in enumerate(random_test_images):
    plt.imshow(x_test_clean[image_idx].reshape(28, 28))
    plt.savefig("original_"+str(image_idx)+".png", cmap="gray")
    plt.imshow(x_test[image_idx].reshape(28, 28))
    plt.savefig("noisy_"+str(image_idx)+".png", cmap="gray")
    plt.imshow(decoded_imgs[image_idx].reshape(28, 28))
    plt.savefig("denoised_"+str(image_idx)+".png", cmap="gray")


